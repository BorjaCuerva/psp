package practica0;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

public class MayusculasPadre {

	public static void main(String[] args) throws IOException {

		System.out.print("Introducir texto: ");
		String texto = utilidades.Entrada.cadena();

		String arg1 = "java";
		String arg2 = "-jar";
		String arg3 = "C:\\Users\\Alumno\\Desktop\\mayusculas.jar";
		//
		// String[] param = {arg1,arg2,arg3,texto};
		String[] param = { arg1, arg2, arg3 };
		Process process = new ProcessBuilder(param).start();

		OutputStream os = process.getOutputStream();
		OutputStreamWriter osw = new OutputStreamWriter(os);
		PrintWriter pw = new PrintWriter(osw);
		pw.print(texto);
		pw.close();
		
		
		InputStream is = process.getInputStream();
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);

		String linea;
		linea = br.readLine();
		System.out.println("Linea:" + linea);
		br.close();

	}

}
