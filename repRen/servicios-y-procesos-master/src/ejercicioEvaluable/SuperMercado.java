package ejercicioEvaluable;

public class SuperMercado {

	public static void main(String[] args) {
		Cliente cliente1 = new Cliente(new int[]{2, 2, 1, 2});
		Cliente cliente2 = new Cliente(new int[]{1, 3, 1, 1, 1, 1});
		Cliente cliente3 = new Cliente(new int[]{1, 1, 1});
		Cliente cliente4 = new Cliente(new int[]{2, 5});

		new Caja("Caja 1",cliente1);
		new Caja("Caja 2",cliente2);
		new Caja("Caja 3",cliente3);
		new Caja("Caja 4",cliente4);
	}

}
