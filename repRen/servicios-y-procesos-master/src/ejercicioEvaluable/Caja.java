package ejercicioEvaluable;

public class Caja implements Runnable {
	Cliente c;
	String nombreCaja;
	Caja(String nombre,Cliente c) {
		this.nombreCaja=nombre;
		this.c=c;
		Thread caja = new Thread(this, "Nuevo Thread");
		caja.start(); // Arranca el nuevo hilo de ejecución. Ejecuta run
	}
	
	
	@Override
	public void run() {
		System.out.println("Inicio de la compra en "+nombreCaja);
		long horaInicio = System.currentTimeMillis();
		for (int i = 0; i < c.getProductos().length; i++) {
			//System.out.println("Cobrando producto: "+i);
			System.out.println(nombreCaja+" procesando producto "+(i+1)+": Tiempo tardado "+(System.currentTimeMillis()-horaInicio)/1000+" segundos");
			
			try {
				Thread.sleep(c.getProductos()[i]*1000);
			} catch (InterruptedException e) {
			}
		}
		long horaFin = System.currentTimeMillis();
		Long tiempoResultado = (horaFin-horaInicio)/1000;
		System.out.println("Se ha tardado "+tiempoResultado+" segundos en procesar todos los productos en la "+nombreCaja );

	}

}
