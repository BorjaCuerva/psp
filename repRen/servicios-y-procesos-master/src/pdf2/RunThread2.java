package pdf2;

class HelloThread2 extends Thread {
	public void run() {
		System.out.println("Hola desde el hilo creado!");
	}
}

public class RunThread2 {
	public static void main(String args[]) {
		new HelloThread2().start();// Crea y arranca un nuevo hilo de ejecución
		System.out.println("Hola desde el hilo principal!");
		System.out.println("Proceso acabando.");
	}
}