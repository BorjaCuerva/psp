package pdf1ej5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class HijoUno {

	public static void main(String args[]) throws IOException {
    	Process process = new ProcessBuilder(args).start(); // se crea el proceso
        
		//Se lee la salida
		InputStream is = (InputStream) process.getInputStream();
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);
		
		String line;
		while ((line = br.readLine()) != null) {
			System.out.println(line);
		}
		br.close();
	}
	
}
