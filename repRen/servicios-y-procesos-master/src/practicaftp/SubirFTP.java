package practicaftp;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.SocketException;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

public class SubirFTP {

	public static void main(String[] args) throws SocketException, IOException {
		FTPClient cliente = new FTPClient();
		String servidor = "localhost";
		// String servidor = "ftp.rediris.es";
		System.out.println("Conecto con el servidor: " + servidor);
		// conectamos cliente FTP al servidor FTP
		cliente.connect(servidor);
		System.out.println(cliente.getReplyString());

		cliente.login("mortadelo", "");
		int respuesta = cliente.getReplyCode();
		if (!FTPReply.isPositiveCompletion(respuesta)) {
			System.out.println("Login no completado: " + respuesta);
			cliente.disconnect();
			System.exit(0);
		}
		boolean transform = cliente.setFileType(FTP.BINARY_FILE_TYPE);
		if (transform) {
			System.out.println("Tipo de transferencia cambiado a binario");
		} else {
			System.out.println("Intento de cambiar tipo de transferencia a binario fallido.");

		}

		FTPFile[] arrayFicheros = cliente.listFiles("/ftp");
		System.out.println(arrayFicheros.length);
		System.out.println();
		for (int i = 0; i < arrayFicheros.length; i++) {

			System.out.println(arrayFicheros[i].getName());

		}

		File file = new File("I:/recibido.txt");
		BufferedInputStream entrada = new BufferedInputStream(new FileInputStream(file));

		cliente.storeFile("/ftp/ficheroTexto4.txt", entrada);



		respuesta = cliente.getReplyCode();
		if (!FTPReply.isPositiveCompletion(respuesta)) {
			System.out.println("Subida de archivo no completada: " + respuesta);
			cliente.disconnect();
			System.exit(0);
		} else {
			System.out.println("Fichero subido con éxito");
		}

		entrada.close();
		cliente.logout();
		cliente.disconnect();

	}

}
