package ejercicio3;

public class Registrador extends Thread {
	
	int numeroLibrosALaMesa;
	
	
	public void run() {
		
		int tamanioMesa = 8;
		int totalLibrosDejados = 0;
		boolean salir = false;
		

		Mesa mesa = new Mesa(tamanioMesa);
		
		while(!salir) {
			System.out.println("Introduce numero de libros a dejar en la mesa: ");
			numeroLibrosALaMesa = utilidades.Entrada.entero();
			
			if(numeroLibrosALaMesa>tamanioMesa || tamanioMesa==0) {
				System.out.println("En la mesa solo entran 8 libros. No hay mas sitio.");
				break;
			}
			
			totalLibrosDejados +=numeroLibrosALaMesa;
			tamanioMesa =mesa.registrarLibro(numeroLibrosALaMesa);
			int numeroLibrosAGuardar = totalLibrosDejados;
			mesa.archivarLibro(numeroLibrosAGuardar);
			
			System.out.println("Quedan por dejar en la mesa : "+tamanioMesa+" libros."+"\n");
			
			
			/*if (tamanioMesa<numeroLibrosALaMesa) {
				salir = true;
				System.out.println("El total de libros que puedes dejar en la mesa es de 8");
			}*/
			
			
		}
			
	}

}
