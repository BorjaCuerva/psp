package ejercicio3;

public class Mesa {
	
	int tamanioMesa = 8;
	
	
	//CONSTRUCTOR
	
	public Mesa(int tamanioMesa) {
		super();
		this.tamanioMesa = tamanioMesa;
	}
	
	public Mesa() {
		super();
		
	}
	
	//GETTERS AND SETTERS
	public int getTamanioMesa() {
		return tamanioMesa;
	}

	public void setTamanioMesa(int tamanioMesa) {
		this.tamanioMesa = tamanioMesa;
	}
	
	
	//METODOS
	public int registrarLibro(int numeroLibrosALaMesa){
		
		System.out.println("He dejado en la mesa: "+numeroLibrosALaMesa+" libros.");
		tamanioMesa = tamanioMesa-numeroLibrosALaMesa;
		
		return tamanioMesa;
		
	}
	
	public void archivarLibro(int numeroLibrosAGuardar){
		
		System.out.println("He archivado: "+numeroLibrosAGuardar+" libros.");
		
	}

}
