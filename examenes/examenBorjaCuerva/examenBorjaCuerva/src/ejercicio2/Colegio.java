package ejercicio2;

public class Colegio extends Thread {
	
	int entradasParaVender = 1240;
	int entradasVendidas;
	String provincia;

	public Colegio() {
		super();
	}

	//METODO RUN

	public void run() {
		
		int entradasMadrid = 0;
		int entradasResto = 0;
		int cnt = 1;
		while (entradasParaVender>entradasVendidas) {
			
			System.out.println("Introduce provincia para el colegio: "+cnt);
			provincia = utilidades.Entrada.cadena().toLowerCase();
			entradasVendidas =(int)Math.floor(Math.random()*(30-45+1)+45);
			
			if (provincia.equals("madrid")) {
				entradasMadrid+=entradasVendidas;
				entradasParaVender = entradasParaVender-entradasVendidas;
			}else {
				entradasResto+=entradasVendidas;
				entradasParaVender = entradasParaVender-entradasVendidas;
			}
			
			cnt++;
		}
		
		System.out.println("En la provincia de Madrid se han vendido "+entradasMadrid+" entradas.");
		System.out.println("En el resto de provincias se han vendido "+entradasResto+" entradas");
		System.out.println("Han quedado: "+entradasParaVender+" entradas sin vender");
		
	}

}
