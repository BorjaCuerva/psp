package ej3;

import java.util.Random;

public class Cliente extends Thread {
	
	int maximo = 100;
	int compraCliente = 0;
	
	public void run() {
		
		compraVasija();
		
	}

	synchronized private void compraVasija() {
		int numeroAleatorio = new Random().nextInt(20) + 1;
		
		if (numeroAleatorio < maximo) {

			maximo -= numeroAleatorio;
			compraCliente += numeroAleatorio;
			
		}
	
	}

	public int getMaximo() {
		return maximo;
	}

	public void setMaximo(int maximo) {
		this.maximo = maximo;
	}

	public int getCompraCliente() {
		return compraCliente;
	}

	public void setCompraCliente(int compraCliente) {
		this.compraCliente = compraCliente;
	}
	
	
	
}
