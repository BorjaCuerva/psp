package ej3;

public class Proveedor extends Thread{
	
	int vasijasAReponer = 0;
	
	public void run() {
		
		reponerVasijas();
		
	}

	synchronized private void reponerVasijas() {
		
		vasijasAReponer = 70;
		
	}

	public int getVasijasAReponer() {
		return vasijasAReponer;
	}

	public void setVasijasAReponer(int vasijasAReponer) {
		this.vasijasAReponer = vasijasAReponer;
	}
	
	

}
