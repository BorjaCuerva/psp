package ej3;

public class ClaseMain {
	
	public static void main(String[] args) {
		

		int stockInicial = 100;
		int stockMinimo = 30;
		int compraCliente = 0;
		
		Cliente cliente = new Cliente();
		Proveedor proveedor = new Proveedor();
		cliente.start();
		proveedor.start();
		
		while (stockInicial >= compraCliente) {
			
			try {
				cliente.sleep(10);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			try {
				proveedor.sleep(10);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			compraCliente += cliente.getCompraCliente();
			System.out.println("El cliente ha comprado "+compraCliente+ " vasijas.");
			stockInicial -= compraCliente;
			
			if (stockInicial <= stockMinimo) {
				stockInicial += proveedor.getVasijasAReponer();
				System.out.println("El proveedor ha repuesto las vasijas.");
			}
			
			
		}
		
		
		
		

	}

}
