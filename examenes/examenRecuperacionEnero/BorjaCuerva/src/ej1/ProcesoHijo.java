package ej1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ProcesoHijo {

	/**
	 * Proceso hijo que se encarga de procesar la informacion del proceso padre
	 * 
	 * @param args
	 */

	public static void main(String[] args) {

		try {
			InputStreamReader isr = new InputStreamReader(System.in);
			BufferedReader br = new BufferedReader(isr);
			String cadena = br.readLine();
			int cnt = 0;
			for (int i = 0; i < cadena.length(); i++) {

				if (cadena.charAt(i) == 46) {
					cnt++;
				}

			}

			System.out.println("La cadena "+cadena+" esta formada por "+cnt+" frases");
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}



	}

}
