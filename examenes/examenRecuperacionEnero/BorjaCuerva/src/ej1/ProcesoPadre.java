package ej1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class ProcesoPadre {
	
	/**
	 * Proceso padre que se encarga de mandar la informacion al proceso hijo
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		
		System.out.println("Introduce frases separadas por . y te dire cuantas frases hay.");
		
		String cadena = utilidades.Entrada.cadena();
		
		String arg1 = "java";
		String arg2 = "-jar";
		String arg3 = "/home/alumno/Escritorio/ej1.jar";
		String[] param = { arg1, arg2, arg3, cadena };
		
		Process process = new ProcessBuilder(param).start();
		OutputStream os = process.getOutputStream();
		OutputStreamWriter osw = new OutputStreamWriter(os);
		BufferedWriter bw = new BufferedWriter(osw);
		bw.write(cadena);
		bw.close();
		InputStream is = process.getInputStream();
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);
		String linea = "";
		while ((linea = br.readLine()) != null) {
			System.out.println(linea);
		}

	}

}
