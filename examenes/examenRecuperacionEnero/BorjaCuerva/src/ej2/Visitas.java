package ej2;

import java.util.Random;

public class Visitas extends Thread {

	int maximo = 40;
	int totalVisitas = 0;

	public void run() {

		accesoASala();

	}

	synchronized private void accesoASala() {

		int numeroAleatorio = new Random().nextInt(40) + 1;

		if (numeroAleatorio < maximo) {

			maximo -= numeroAleatorio;
			totalVisitas += numeroAleatorio;
		}

	}

	public int getMaximo() {
		return maximo;
	}

	public void setMaximo(int maximo) {
		this.maximo = maximo;
	}

	public int getTotalVisitas() {
		return totalVisitas;
	}

	public void setTotalVisitas(int totalVisitas) {
		this.totalVisitas = totalVisitas;
	}

}
