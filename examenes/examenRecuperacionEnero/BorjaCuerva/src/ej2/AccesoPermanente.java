package ej2;

import java.util.Random;

public class AccesoPermanente extends Thread {

	int maximo = 40;
	int totalPermanente = 0;

	public void run() {

		accesoASala();

	}

	synchronized private void accesoASala() {

		int numeroAleatorio = new Random().nextInt(40) + 1;

		if (numeroAleatorio < maximo) {

			maximo -= numeroAleatorio;
			totalPermanente += numeroAleatorio;
		}

		// System.out.println("Numero de personas permanente:" + total);
		

	}

	public int getMaximo() {
		return maximo;
	}

	public void setMaximo(int maximo) {
		this.maximo = maximo;
	}

	public int getTotalPermanente() {
		return totalPermanente;
	}

	public void setTotalPermanente(int totalPermanente) {
		this.totalPermanente = totalPermanente;
	}

}
