package ej2;

import java.util.Random;

public class AccesoTemporal extends Thread {

	int maximo = 40;
	int totalTemporal = 0;

	public void run() {

		accesoASala();

	}

	synchronized private void accesoASala() {

		int numeroAleatorio = new Random().nextInt(40) + 1;

		if (numeroAleatorio < maximo) {

			maximo -= numeroAleatorio;
			totalTemporal += numeroAleatorio;
		}

	}

	public int getMaximo() {
		return maximo;
	}

	public void setMaximo(int maximo) {
		this.maximo = maximo;
	}

	public int getTotalTemporal() {
		return totalTemporal;
	}

	public void setTotalTemporal(int totalTemporal) {
		this.totalTemporal = totalTemporal;
	}

}
