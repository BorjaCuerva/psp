package ej2;

public class ClaseMain {

	public static void main(String[] args) {

		int maximo = 135;
		int gentePermanente = 0;
		int genteTemporal = 0;
		int genteVisitas = 0;
		int genteTotal = 0;

		AccesoPermanente personalPermanente = new AccesoPermanente();
		AccesoTemporal personalTemporal = new AccesoTemporal();
		Visitas personalVisitas = new Visitas();

		personalPermanente.start();
		personalTemporal.start();
		personalVisitas.start();
		
		while (maximo > genteTotal) {
			
			// PERSONAL ACCESOPERMANENTE
			gentePermanente += personalPermanente.getTotalPermanente();
			try {
				AccesoPermanente.sleep(10);
			} catch (InterruptedException e) {
				System.out.println("Error con el hilo AccesoTemporal " + e);
				e.printStackTrace();
			}

			// PERSONAL ACCESOTEMPORAL

			genteTemporal += personalTemporal.getTotalTemporal();
			

			try {
				AccesoTemporal.sleep(10);
			} catch (InterruptedException e) {
				System.out.println("Error con el hilo AccesoTemporal " + e);
				e.printStackTrace();
			}

			// PERSONAL VISITAS

			genteVisitas += personalVisitas.getTotalVisitas();
			
			try {
				Visitas.sleep(10);
			} catch (InterruptedException e) {
				System.out.println("Error con el hilo Visitas " + e);
				e.printStackTrace();
			}

			genteTotal += gentePermanente + genteTemporal + genteVisitas;
			maximo -= gentePermanente + genteTemporal + genteVisitas;
			
		}

		System.out.println("De tipo Acceso Permanente han entrado " + gentePermanente);
		System.out.println("De tipo Acceso temporal han entrado "+genteTemporal);
		System.out.println("De tipo Visitas han entrado "+genteVisitas);

	}

}
