package preExamenDataGram;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class Server {
	public static void main(String[] args)  throws IOException, ClassNotFoundException {
		
		DatagramSocket ds = new DatagramSocket(4000);
		byte[] buf = new byte[1000];
		DatagramPacket dgp = new DatagramPacket(buf, buf.length);
		ds.receive(dgp);
	      String rcvd = new String(dgp.getData(), 0, dgp.getLength()) + ", from address: "
	              + dgp.getAddress() + ", port: " + dgp.getPort();
	          System.out.println(rcvd);
		
	}
}
