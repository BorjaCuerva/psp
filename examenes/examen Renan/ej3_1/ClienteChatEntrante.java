package ej3_1;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;

public class ClienteChatEntrante implements Runnable {

	
	
	private Socket serverConectado;
	
	public ClienteChatEntrante(Socket clienteConectado) {
		this.serverConectado = clienteConectado;
		Thread t = new Thread(this, "ThreadSecundario");
		t.start();
	}

	@Override
	public void run() {
		try {
			ObjectInputStream is = new ObjectInputStream(serverConectado.getInputStream());
			
			while (true) {

				Mensaje m = (Mensaje) is.readObject();
				System.out.println(m.getNombreUsuario() +" "+m.getFechaYHoraConFormato() +": " + m.getMensaje());
			}
			
		} catch (IOException | ClassNotFoundException e) {
			System.out.println("Servidor de chat completo, cerrando aplicación.");
			System.exit(0);
		}

		System.out.println("Finalizando hilo");

	}

}
