package ej3_1;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Date;
import java.util.Observable;
import java.util.Observer;

public class ClienteConectado extends Thread implements Observer {

	Socket clienteConectado;
	ObjectInputStream ois;
	ObjectOutputStream oos;
	Mensaje mensajes;

	public ClienteConectado(Socket clienteConectado, Mensaje mensajes) {
		this.mensajes = mensajes;
		this.clienteConectado = clienteConectado;
		try {
			this.ois = new ObjectInputStream(clienteConectado.getInputStream());
			this.oos = new ObjectOutputStream(clienteConectado.getOutputStream());
		} catch (IOException ex) {
			System.out.println("Fallo en constructor del hilo: ");
		}
		System.out.println("Constructor del hilo finalizado");
	}

	public void run() {
		Mensaje m;
		Mensaje mensaje = new Mensaje("b", "a");
		String mensajeBienvenida = "Bienvenid@ al chat";
		mensajes.addObserver(this);

		boolean conectado = true;
		try {
		//	m = (Mensaje) ois.readObject();
		//	sleep(200);
			//mensajes.setMensaje(new Mensaje("BIENVENUE", "Servidor"));
			
			
			m = (Mensaje) ois.readObject();
			m.setFechaYHora(new Date());
			m.setMensaje(mensajeBienvenida+" "+m.getNombreUsuario());
			m.setNombreUsuario("Servidor");
			mensajes.actualizarMensaje(m);
			
			
			
			while (conectado) {
				
				m = (Mensaje) ois.readObject();
				m.setFechaYHora(new Date());
				mensajes.actualizarMensaje(m);
				//mensajes.setMensaje(mensaje);
				
				if (m.getMensaje().equalsIgnoreCase("fin")) {
					conectado = false;
				}

			}
		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			try {
				ois.close();
				System.out.println("Cerrando canuto de entrada"+ e.getMessage());
			} catch (IOException e1) {
				System.out.println("Fallo al cerrar canuto de entrada");
			}
			try {
				oos.close();
				System.out.println("Cerrando canuto de salida"+ e.getMessage());
			} catch (IOException e1) {
				System.out.println("Fallo al cerrar canuto de salida");
			} finally {
				try {
					Servidor.numClientesConectados--;
					mensajes.deleteObserver(this);
					clienteConectado.close();
					System.out.println("Cerrando socket"+ e.getMessage());
				} catch (IOException a) {
					System.out.println("Fallo al cerrar socket");
				}
			}
		}
	}


	@Override
	public void update(Observable o, Object arg) {
		System.out.println("CAMBIO EN HILO");
		try {
			oos.writeObject((Mensaje) arg);
		} catch (IOException e) {
			Servidor.numClientesConectados--;
			mensajes.deleteObserver(this);
			//e.printStackTrace();
		}
	}

}
