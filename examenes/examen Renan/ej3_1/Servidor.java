package ej3_1;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class Servidor {

	public static int numClientesConectados = 0;

	public static void main(String[] args) {

		Mensaje mensajes = new Mensaje();
		Mensaje mensajeServidorCompleto = new Mensaje("Imposible conectar. Servidor lleno", "Servidor");

		int numMaxConectados = 2;
		int nPuerto = 1313; // puerto local
		ServerSocket servidor = null;
		Socket clienteConectado = null;

		try {
			servidor = new ServerSocket(nPuerto); // throws IOException

			while (true) {
				System.out.println("Esperando al cliente....");
				System.out.println("Clientes conectados: "+numClientesConectados);
				clienteConectado = servidor.accept(); // throws IOException

				if (numClientesConectados >= numMaxConectados) {
					ObjectOutputStream oos = new ObjectOutputStream(clienteConectado.getOutputStream());
					oos.writeObject(mensajeServidorCompleto);
					oos.close();
				} else {
					ClienteConectado conexionCliente = new ClienteConectado(clienteConectado, mensajes);
					conexionCliente.start();
					numClientesConectados++;
				}

//			ObjectInputStream ois = new ObjectInputStream(clienteConectado.getInputStream());
//			ObjectOutputStream oos = new ObjectOutputStream(clienteConectado.getOutputStream());

			}
		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			try {
				clienteConectado.close();
				servidor.close();
			} catch (IOException ex) {
			}
		}
	}

}
