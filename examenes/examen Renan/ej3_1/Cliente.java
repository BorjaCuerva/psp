package ej3_1;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;

import utilidades.Entrada;

public class Cliente {

	@SuppressWarnings("resource")
	public static void main(String[] args) {
		String host = "localhost";
		int nPuerto = 1313; // puerto remoto
		Socket cliente = null;

		String entrada;
		int contador = 1;

		String nick = "Renan";
		try {
			cliente = new Socket(host, nPuerto); // throws IOException &
			// Thread.sleep(500);
			new ClienteChatEntrante(cliente);// ABRIMOS HILO MENSAJE ENTRANTE
			ObjectOutputStream oos = new ObjectOutputStream(cliente.getOutputStream());

			Mensaje mensaje = new Mensaje("", nick);
			oos.writeObject(mensaje);

			// Mensaje MensajeSaludo = new Mensaje("", nick);
			// oos.writeObject(MensajeSaludo);
			while (true) {
				System.out.println(nick + " escribe el mensaje " + contador + "(\"fin\" para salir):");

				entrada = Entrada.cadena();

				mensaje = new Mensaje(entrada, nick);

				oos.writeObject(mensaje);

				if (entrada.equalsIgnoreCase("fin")) {

					oos.close();
					cliente.close();
					System.exit(0);
				} else {
					contador++;
				}

			}
		} catch (Exception e) {
		//	System.out.println("dew");
			try {
				Thread.sleep(50);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			System.exit(0);
		}
	}

}
