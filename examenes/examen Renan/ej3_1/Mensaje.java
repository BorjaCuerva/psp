package ej3_1;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Observable;

public class Mensaje extends Observable implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6303583365935494508L;
	private String mensaje;
	private String nombreUsuario;
	private Mensaje m;
	private Date fechaYHora;
	
	

	public Mensaje(String mensaje, String nombreUsuario) {
		super();
		this.mensaje = mensaje;
		this.nombreUsuario = nombreUsuario;
	}

	public Mensaje() {
	}

	public String getMensaje() {
		return mensaje;
	}
	
	public Mensaje getM() {
		return m;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	public void actualizarMensaje(Mensaje mens) {
		this.m = mens;
		// Indica que el mensaje ha cambiado
		this.setChanged();
		// Notifica a los observadores que el mensaje ha cambiado y se lo pasa
		// (Internamente notifyObservers llama al metodo update del observador)
		this.notifyObservers(this.getM());
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public Date getFechaYHora() {
		return fechaYHora;
	}

	public String getFechaYHoraConFormato() {
		String patronHorario = "(d MMM yyyy HH:mm)";
		SimpleDateFormat sdf = new SimpleDateFormat(patronHorario);
		return sdf.format(fechaYHora);
	}
	
	public void setFechaYHora(Date fechaYHora) {
		this.fechaYHora = fechaYHora;
	}

}
