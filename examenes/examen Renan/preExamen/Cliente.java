package preExamen;


import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;



public class Cliente {

	public static void main(String[] args) throws UnknownHostException, IOException {
		Socket connect = null;
		connect = new Socket("localhost",5000);
		OutputStream os= connect.getOutputStream();
		Mensaje m = new Mensaje();
		m.setMensaje("Hola vida");
		ObjectOutputStream oos = new ObjectOutputStream(os);
		oos.writeObject(m);
		connect.close();
	}

}

