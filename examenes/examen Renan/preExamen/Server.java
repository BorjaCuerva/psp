package preExamen;


import java.io.IOException;
import java.io.InputStream;

import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;



public class Server {
	
	public static void main(String[] args)  throws IOException, ClassNotFoundException {
		ServerSocket server = null;
		int puerto = 5000;
		server = new ServerSocket(puerto);
		Socket cliente = server.accept();
		InputStream is = cliente.getInputStream();
		ObjectInputStream ois = new ObjectInputStream(is);
		Mensaje m = (Mensaje)ois.readObject();
		System.out.println(m.getMensaje());
		server.close();

	}

}
