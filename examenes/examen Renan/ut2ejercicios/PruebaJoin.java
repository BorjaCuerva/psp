package ut2ejercicios;

public class PruebaJoin {

	public static void main(String[] args) throws InterruptedException {

		for (int i = 0; i < 5; i++) {
			HiloJoin hilo = new HiloJoin(i + 1);
			hilo.start();
			hilo.join();
		}
		System.out.println("Todos los hilos creados");
	}

}
