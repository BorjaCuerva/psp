package ut2ejercicios;

public class HiloReloj {

	public static void main(String[] args) throws InterruptedException {
		HiloTic tic = new HiloTic();
		HiloTac tac = new HiloTac();
		
		tic.start();
		Thread.sleep(1000);
		tac.start();

	}

}
