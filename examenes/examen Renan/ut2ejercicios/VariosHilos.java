package ut2ejercicios;

public class VariosHilos {

	public static void main(String[] args) throws InterruptedException {

		for (int i = 0; i < 20; i++) {
			HiloC hilo = new HiloC(i + 1);
			hilo.start();
		}
		System.out.println("Todos los hilos creados");
	}

}
