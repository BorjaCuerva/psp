package ut2ejercicios;

public class HiloCR implements Runnable {

	int c;

	public HiloCR(int c) {
		this.c = c;
		Thread t = new Thread(this);
		System.out.println("Creando hilo: " + this.c);
		t.start();
	}

	@Override
	public void run() {
		for (int i = 0; i < 5; i++) {
			System.out.println("Hilo " + this.c + " línea " + (i + 1));
		}
	}

}
