package tarroGalletas;

import java.util.concurrent.Semaphore;

public class TarroGalletas {

	public static void main(String[] args) throws InterruptedException {
		TarroGalletas tarro = new TarroGalletas(10);
		Semaphore noHayGalletas = new Semaphore(0);
		Semaphore hayGalletas = new Semaphore(0);
		Semaphore mutex = new Semaphore(1);

		Ninio ninio1 = new Ninio(noHayGalletas, hayGalletas, mutex,tarro);
		Ninio ninio2 = new Ninio(noHayGalletas, hayGalletas, mutex,tarro);
	}

	private int galletas;

	public TarroGalletas(int galletas) {
		super();
		this.galletas = galletas;
	}

	public int getGalletas() {
		return galletas;
	}

	public void setGalletas(int galletas) {
		this.galletas = galletas;
	}

}
