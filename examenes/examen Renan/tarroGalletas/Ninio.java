package tarroGalletas;

import java.util.concurrent.Semaphore;

public class Ninio implements Runnable {

	private Semaphore noHayGalletas;
	private Semaphore hayGalletas;
	private Semaphore mutex;
	private TarroGalletas tarro;

	public Ninio(Semaphore noHayGalletas, Semaphore hayGalletas, Semaphore mutex, TarroGalletas tarro) {
		super();
		this.noHayGalletas = noHayGalletas;
		this.hayGalletas = hayGalletas;
		this.mutex = mutex;
		this.tarro = tarro;
	}

	@Override
	public void run() {
		while (true) {
			try {
				mutex.acquire();

				if (tarro.getGalletas() == 0) {
					noHayGalletas.release();
					hayGalletas.acquire();
					tarro.getGalletas();
				}
			} catch (InterruptedException e) {
			}
		}
	}

}
