package ej3;

public class Minusculas implements Runnable {

	private String cadena;

	public Minusculas(String cadena) {
		try {
			this.cadena = cadena;
			Thread t = new Thread(this);
			t.start();
			t.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		System.out.println("HILO MINÚSCULAS DICE: " + cadena.toLowerCase());

	}

}
