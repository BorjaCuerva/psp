package ej3;

import utilidades.Entrada;

public class CadenasDeTexto {

	public static void main(String[] args) {
		boolean salir = false;
		do {
			System.out.print("Introduzca una cadena de texto FIN para salir: ");
			String cadena = Entrada.cadena();
			if (cadena.equals("FIN")) {
				salir = true;
				return;
			}
			if (cadena.charAt(0) > 64 && cadena.charAt(cadena.length() - 1) <= 90) {
				new Minusculas(cadena);
			} else {
				try {
					Mayusculas mayusculas = new Mayusculas(cadena);
					mayusculas.start();
					mayusculas.join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		} while (!salir);
	}

}
