package ej4;

public class Cliente {

	private Cuenta cuenta;
	private String nombre;
	private int cantidad;
	private int total;

	public Cliente(Cuenta cuenta, String nombre, int cantidad, int total) {
		super();
		this.cuenta = cuenta;
		this.nombre = nombre;
		this.cantidad = cantidad;
		this.total = total;
	}

	public Cuenta getCuenta() {
		return cuenta;
	}

	public void setCuenta(Cuenta cuenta) {
		this.cuenta = cuenta;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

}
