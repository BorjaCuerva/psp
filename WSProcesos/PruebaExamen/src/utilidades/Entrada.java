package utilidades;

import java.io.*;

public class Entrada {
	static String inicializar() {
		String buzon = "";
		InputStreamReader flujo = new InputStreamReader(System.in);
		BufferedReader teclado = new BufferedReader(flujo);
		try {
			buzon = teclado.readLine();
		} catch (Exception e) {
			System.out.append("Entrada incorrecta)");
		}
		return buzon;
	}

	public static int entero() {
		int valor = Integer.parseInt(inicializar());
		return valor;
	}

	public static double real() {
		double valor = Double.parseDouble(inicializar());
		return valor;
	}

	public static String cadena() {
		String valor = inicializar();
		return valor;
	}

	public static char caracter() {
		String valor = inicializar();
		return valor.charAt(0);
	}
}


/*
 
public class MostrarTotalCaracteres {
public static void main(String[] args) {
		System.out.print("Introduce una cadena: ");
		String cadena = Entrada.cadena();
		try {
			String arg1 = "java";
			String arg2 = "-jar";
			String arg3 = "C:\\Users\\Borja\\Desktop\\ej1.jar";
			String[] param = { arg1, arg2, arg3, cadena };
			
			Process process = new ProcessBuilder(param).start();
			OutputStream os = process.getOutputStream();
			OutputStreamWriter osw = new OutputStreamWriter(os);
			BufferedWriter bw = new BufferedWriter(osw);
			bw.write(cadena);
			bw.close();
			InputStream is = process.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			String linea = "";
			while ((linea = br.readLine()) != null) {
				System.out.println(linea);
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

**********************************************************************************
public class ContarVocales {
	public static void main(String[] args) {
		try {
			InputStreamReader isr = new InputStreamReader(System.in);
			BufferedReader br = new BufferedReader(isr);
			String cadena = br.readLine();
			int cnt = 0;
			for (int i = 0; i < cadena.length(); i++) {
				if (cadena.charAt(i)=='a' || cadena.charAt(i)=='e' || cadena.charAt(i)=='i' || cadena.charAt(i)=='o' || cadena.charAt(i)=='u') {
					cnt++;
				}	
			}
			System.out.println("La cadena '" + cadena + "' tiene " + cnt + " vocales.");
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


 */







