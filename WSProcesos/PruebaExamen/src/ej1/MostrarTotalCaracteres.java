package ej1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import utilidades.Entrada;

public class MostrarTotalCaracteres {

	public static void main(String[] args) {
		System.out.print("Introduce una cadena: ");
		String cadena = Entrada.cadena();
		try {
			String arg1 = "java";
			String arg2 = "-jar";
			String arg3 = "C:\\Users\\Borja\\Desktop\\ej1.jar";
			String[] param = { arg1, arg2, arg3, cadena };
			
			Process process = new ProcessBuilder(param).start();
			OutputStream os = process.getOutputStream();
			OutputStreamWriter osw = new OutputStreamWriter(os);
			BufferedWriter bw = new BufferedWriter(osw);
			bw.write(cadena);
			bw.close();
			InputStream is = process.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			String linea = "";
			while ((linea = br.readLine()) != null) {
				System.out.println(linea);
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
