package ejemplosClaseImplements;

/**
 * Usando implements
 * @author alumno
 *
 */
public class HelloTread implements Runnable {
	
	
	
	Thread t; //Hilo
	
	/**
	 * Constructor por defecto
	 */
	
	HelloTread() {
		t = new Thread(this, "Nuevo Thread");
		System.out.println("1.Creado hilo: " + t);
		t.start(); // Arranca el nuevo hilo de ejecución. Ejecuta run automaticamente
	}
	
	/**
	 * Este metodo se ejecuta automaticamente al usar start();
	 */
	public void run() {
		System.out.println("2.Hola desde el hilo creado!");
		System.out.println("3.Hilo finalizando.");
	}

}
