package ejercicio1_3;

public class HiloReloj {

	public static void main(String[] args) throws InterruptedException {
		
		HiloTic hiloTic = new HiloTic();
		HiloTac hiloTac = new HiloTac();
		hiloTic.start();
		Thread.sleep(1000);//Pausa de 1 segundo para que no se lancen los dos a la vez
		hiloTac.start();
	
	}

}
