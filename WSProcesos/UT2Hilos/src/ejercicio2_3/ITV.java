package ejercicio2_3;

import java.util.ArrayList;
import java.util.Random;

public class ITV {

	public static void main(String[] args) {
		
		//Numeros aleatorios para los puestos (1-5)
		//Numeros aleatorios para los Vehiculos (20-50)
		//Numeros aleatorios para el tiempo de espera (10-100) en milisegundos
		
		Random random = new Random();
		int numeroPuestos =  (int)(random.nextDouble() * 5 + 1);
		int numeroVehiculos =  (int)(random.nextDouble() * 30 + 20);
		int tiempo = (int)(random.nextDouble()*90+10);
		//ArrayList para almacenar los vehiculos
		ArrayList <Vehiculo> listaVehiculo = new ArrayList<>();

		//Si solo hay 1 puesto de inspeccion lo ponemos en singular.
		if (numeroPuestos == 1) {
			System.out.println("Hay "+numeroVehiculos+" Vehículos que serán atendidos por "+numeroPuestos+" puesto de inspección.");
		}else {
			System.out.println("Hay "+numeroVehiculos+" Vehículos que serán atendidos por "+numeroPuestos+" puestos de inspección.");
		}
		
		//for para crear Threads vehiculos
		for (int i = 0; i < listaVehiculo.size(); i++) {
			Vehiculo vehiculo = new Vehiculo(numeroPuestos, numeroVehiculos);
			listaVehiculo.add(vehiculo);
		}
		
		
		
		
		
	}

}
