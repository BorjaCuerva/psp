package ejercicio2_3;

public class Vehiculo extends Thread {
	
	private int numeroVehiculo;
	private int numeroPuesto;
	
	/*
	 * Constructor
	 */
	public Vehiculo (int numeroPuesto, int numeroVehiculo) {
		this.numeroPuesto = numeroPuesto;
		this.numeroVehiculo = numeroVehiculo;	
	}
	
	
	public void run() {
		
		System.out.println("El puesto "+numeroPuesto+ " ha atendido al vehículo "+numeroVehiculo);
		
	}
	
}
