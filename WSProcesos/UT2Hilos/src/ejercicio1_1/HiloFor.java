package ejercicio1_1;

/**
 * 
 * @author Borja
 * estiende de Thread que es mas sencillo que implements Runnable
 *
 */
public class HiloFor extends Thread{
	
	//Creamos el metodo run que se ejecuta automaticamente.
	//Lo llamamos desde la clase DentroFuera
	public void run() {
		for (int i = 0;i<=10;i++) {
			System.out.println("Estoy dentro del hilo");
		}
	}
	

}
