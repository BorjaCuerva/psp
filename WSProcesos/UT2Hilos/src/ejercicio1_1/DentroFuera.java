package ejercicio1_1;

public class DentroFuera {

	public static void main(String[] args) {
		
		HiloFor hilo = new HiloFor(); //Creamos objeto de la clase HiloFor
		hilo.start(); //Lo ejecutamos con start. Que llama al metodo run() de HiloFor
		
		for (int i = 0;i<=10;i++) {
			System.out.println("Estoy fuera del hilo");
		}

	}

}
