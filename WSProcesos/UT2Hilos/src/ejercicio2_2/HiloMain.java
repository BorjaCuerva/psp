package ejercicio2_2;

public class HiloMain {

	public static void main(String[] args) {
		//Creamos los objetos de tipo Hilo
		//primer numero es el numero de hilo, segundo numero de incremento o decremento
		Hilo hilo1 = new Hilo(1, 1); 
		Hilo hilo2 = new Hilo(2, -1);
		
		//iniciamos los procesos que llaman al metodo run() de la clase Hilo
		hilo1.start();
		hilo2.start();

	}

}
