package ejercicio2_2;

public class Hilo extends Thread {
	
	private int hilo; //numero del hilo
	private int numero; //numero a incrementar o decrementar
	static int cuenta = 0;
	
	/**
	 * Constructor
	 * @param hilo numero del hilo
	 * @param numero numero a incrementar o decrementar
	 */
	public Hilo(int hilo,int numero) {
		this.hilo = hilo;
		this.numero = numero;
	}
	
	
	public void run() {
		//1000 vueltas
		for (int i=0;i<1000;i++){
			Hilo.cuenta = Hilo.cuenta + numero;
			System.out.println ("Hilo: "+hilo+" Resultado: " + Hilo.cuenta);
		}
		
	}

}
