package ejercicio1_2;

public class HiloC extends Thread{

	int c; //Valor pasado como parametro
	int x = 1;//Numero de linea
	
	/**
	 * Constructor que genera una frase con el numero de hilo
	 * @param c es el numero de hilo
	 */
	public HiloC(int c) {
		super();
		this.c = c;
		System.out.println("Creando hilo "+c);
	}
		
	/**
	 * Se ejecuta automaticamente desde el VariosHilos al ejecutar start()
	 * Sacamos el numero de hilo y el numero de linea
	 */
	public void run() {
		for (int i = 0;i<=5;i++) {
			System.out.println("Hilo "+c+" linea "+x);
			x++;
		}
	}
	
	
}
