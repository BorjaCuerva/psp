package ejercicio1_2;

public class VariosHilos {

	public static void main(String[] args) {
			
		/**
		 * Creamos 5 objetos de la clase hilo y le pasamos como parametro el
		 * numero del hilo
		 * con start() llamamos al run() de la clase HiloC
		 */
			HiloC hilo1 = new HiloC(1);
			hilo1.start();
			HiloC hilo2 = new HiloC(2);
			hilo2.start();
			HiloC hilo3 = new HiloC(3);
			hilo3.start();
			HiloC hilo4 = new HiloC(4);
			hilo4.start();
			HiloC hilo5 = new HiloC(5);
			hilo5.start();

		System.out.println("Todos los hilos creados.");
		
	}

}
