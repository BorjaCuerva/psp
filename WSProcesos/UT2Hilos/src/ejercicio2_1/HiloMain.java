package ejercicio2_1;

public class HiloMain {

	public static void main(String[] args) {
		//Creamos los objetos de tipo Hilo
		//primer numero es el numero de hilo, segundo numero de incremento o decremento
		Hilo hilo1 = new Hilo(1, 1); 
		Hilo hilo2 = new Hilo(2, -1);
		Hilo hilo3 = new Hilo(3, 1);
		Hilo hilo4 = new Hilo(4, -1);
		
		//iniciamos los procesos que llaman al metodo run() de la clase Hilo
		hilo1.start();
		hilo2.start();
		hilo3.start();
		hilo4.start();

	}

}
