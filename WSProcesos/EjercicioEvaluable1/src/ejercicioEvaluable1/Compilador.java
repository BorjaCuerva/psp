package ejercicioEvaluable1;

import java.io.File;

public class Compilador {

	public static void main(String[] args) {
		
		try {
			new ProcessBuilder(args).start();
			File ruta = new File(args[1]);
			
			if (!ruta.exists()) {
				System.err.print("¡¡La ruta no existe!!");
			}
			
			if (ruta.isDirectory()) {
				System.err.print("¡¡No puedes seleccionar un fichero!!");
			}else {
				if (ruta.exists()) {
					System.out.println("Compilación realizada correctamente");
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
			System.err.print("¡No se ha compilado el archivo!");
		}
		
	}

}
