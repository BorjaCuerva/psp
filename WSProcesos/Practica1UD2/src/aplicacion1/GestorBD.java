package aplicacion1;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.swing.JOptionPane;

public class GestorBD {

	Connection con;

	public void conectarBD() {

		try {
			Class.forName("com.mysql.jdbc.Driver");
			//Conectamos a la BBDD mysql
			con = DriverManager.getConnection("jdbc:mysql://localhost/BBDD_PSP_1", "root", "manager");
			
		} catch (ClassNotFoundException e) {
			JOptionPane.showMessageDialog(null, "Error con el driver", "Driver", 1);
			e.printStackTrace();
		} catch (SQLException ex) {
			JOptionPane.showMessageDialog(null, "Error al conectar a la BBDD", "BBDD", 1);
			ex.printStackTrace();
		}

	}
	
}
