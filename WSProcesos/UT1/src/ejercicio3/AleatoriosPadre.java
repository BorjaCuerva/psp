package ejercicio3;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;


/**
 * La clase padre llama a la clase hijo.
 * Antes hay que crear el runnable del hijo
 * @author Borja
 *
 */
public class AleatoriosPadre {

	public static void main(String[] args) {
		try {
			String entrada = "";
			while (entrada != null) {
				//Estos 4 pasos hay que hacerlos en todos
				Process process = new ProcessBuilder(args).start(); //Creamos proceso y selecciona argumentos
				InputStream is = process.getInputStream();
				InputStreamReader isr = new InputStreamReader(is);
				BufferedReader br = new BufferedReader(isr);
				
				//Algoritmo 
				String linea;
				linea = br.readLine();
				System.out.println("Numero Aleatorio: " + linea);
				br.close();
				System.out.print("Introduzca Enter para generar otro numero. FIN para terminar: ");
				entrada = Entrada.cadena().toLowerCase().trim();
				if (entrada.equals("fin")) {
					entrada = null;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
