package ejercicio3;

import java.util.Random;

public class AleatoriosHijo {
	
	/**
	 * Clase hija que crea aleatorios
	 */
	public static void main(String[] args) {
		
		int numero;
		Random aleatorio = new Random();
		numero = aleatorio.nextInt(11);
		System.out.println(numero);

	}

}
