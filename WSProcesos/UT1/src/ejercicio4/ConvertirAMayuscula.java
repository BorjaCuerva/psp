package ejercicio4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Clase que lee por teclado y transforma a mayusculas
 * PROCESO HIJO
 * @author Borja
 *
 */
public class ConvertirAMayuscula {

	public static void main(String[] args) {
		try {
			//Leemos por teclado (Es igual que la clase Entrada)
			BufferedReader bfr = new BufferedReader(new InputStreamReader(System.in));
			String linea;
			while ((linea = bfr.readLine()) != null) {
				System.out.println(linea.toUpperCase());
			}
			bfr.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
