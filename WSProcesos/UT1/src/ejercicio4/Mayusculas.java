package ejercicio4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Arrays;

public class Mayusculas {
	
	/**
	 * PROCESO PADRE
	 * Manda informacion al hijo con OutputSream
	 * Recoge la informacion que proporciona el hijo con InputStream
	 * @param args
	 */

	public static void main(String[] args) {
		
		System.out.println("Introduce palabra");
		String palabra = Entrada.cadena();

		//Proceso hijo
		try {
			Process hijo = new ProcessBuilder(args).start(); //Iniciamos proceso hijo
			OutputStream os = hijo.getOutputStream();
			OutputStreamWriter osw = new OutputStreamWriter(os);
			PrintWriter pw = new PrintWriter(osw);
			pw.print(palabra); //Mandamos la palabra al proceso hijo
			pw.close(); //Cerramos el buffer
			
			//El proceso padre recoge la salida del proceso hijo
			InputStream is = hijo.getInputStream(); //recoge proceso hijo
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader bfr = new BufferedReader(isr); //Guardamos contenido en buffer
			String linea;
			System.out.println("Salida del proceso " + Arrays.toString(args) + ":");
			while ((linea = bfr.readLine()) != null) {
				System.out.println(linea); //Sacamos por pantalla la palabra transformada a mayuscula
			}
			bfr.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
