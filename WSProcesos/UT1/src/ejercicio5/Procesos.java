package ejercicio5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;

public class Procesos {

	public static void main(String[] args) {
		
		Runtime runtime = Runtime.getRuntime();
		
		try {
			Process proceso1 = runtime.exec("firefox"); //ejecuta firefox
			Process proceso2 = runtime.exec("ls -R"); //muestra archivos con toda la informacion y los ocultos
			
			//Para ejecutar el proceso 1
			InputStream is = proceso1.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader bfr = new BufferedReader(isr);
			
			//Proceso 2
			OutputStream os2 = proceso2.getOutputStream();
			PrintWriter pw = new PrintWriter(os2);
			
			String linea;
			while ((linea = bfr.readLine())!=null) {
				pw.write(linea +"\n");
				linea = bfr.readLine();
			}
			
			bfr.close();
			pw.close();
			is.close();
			isr.close();
			os2.close();
			
			InputStream is2 = proceso2.getInputStream();
			InputStreamReader isr2 = new InputStreamReader(is2);
			BufferedReader bfr2 = new BufferedReader(isr2);
			
			String linea2;
			while ((linea2 = bfr2.readLine())!=null) {
				System.out.println(linea2);
				linea2 = bfr2.readLine();
			}
			bfr2.close();
			is2.close();
			isr2.close();
			

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
