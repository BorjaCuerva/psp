package ejercicio1;

import java.sql.SQLException;
import java.util.Random;

import utilidades.GestorBD;

public class Hilos extends Thread {
	
	public void run() {
		int res = ingresos(); 
		try {
			
			GestorBD.registrar(res); 
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	//Ingresos aleatorios
	private static int ingresos() {
		Random r = new Random();	
		int al = r.nextInt(990)+10;	
		return al;
	}

}
