package ejercicio1;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Random;

import utilidades.Entrada;
import utilidades.GestorBD;

/**
 * 
 * @author Borja
 *
 */
public class Aplicacion1 {

	public static void main(String[] args) throws Exception {
		
		java.sql.Connection con = null;
		GestorBD bd = new GestorBD();
		con = bd.conectarBBDD();

		System.out.println("Numero de registros a insertar: ");
		int numeroRegistros = Entrada.entero();
		
		System.out.println("Numero de hilos a insertar: ");
		int hilos = Entrada.entero();
		
		ArrayList<Hilos> arrayListHilos = new ArrayList<Hilos>();
		
		for (int i = 0; i < hilos; i++) {
			arrayListHilos.add(new Hilos());
		}
		
		int contadorHilos = 1;
		
		while (numeroRegistros > contadorHilos || numeroRegistros == contadorHilos) {
			for (int j = 0; j < arrayListHilos.size(); j++) {
				arrayListHilos.get(j).run();
				System.out.println("Hilo "+(j+1)+", Registro "+contadorHilos);			
				if (contadorHilos == numeroRegistros) {
					System.exit(0);
				}
				contadorHilos++;
			}
		}
		
	}

}
