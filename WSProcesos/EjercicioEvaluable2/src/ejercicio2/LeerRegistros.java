package ejercicio2;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import utilidades.GestorBD;

public class LeerRegistros {

	public static void main(String[] args) throws Exception {

		java.sql.Connection con = null;
		GestorBD g = new GestorBD();
		con = g.conectarBBDD();
		
		leerRegistros(con);

	}
	
	/**
	 * Metodo para leer todos los registros y hacer una suma total de todos los ingresos.
	 * Mide el tiempo que ha tardado.
	 * @param con --> Conexion
	 * @throws SQLException
	 * @throws InterruptedException
	 */
	private static void leerRegistros(Connection con) throws SQLException, InterruptedException {
		
		long inicio = System.nanoTime();
		
		java.sql.Connection conexion = con;
		String select = "SELECT * FROM T_INDIVIDUOS";
		java.sql.Statement st = conexion.createStatement();
		ResultSet rs = st.executeQuery(select);
		ArrayList<Registros> arrayListRegistros = new ArrayList<Registros>();
		int cnt = 0;
		while (rs.next()) {
			arrayListRegistros.add(new Registros(rs.getInt(1), rs.getString(2), rs.getInt(3)));
			cnt = cnt + Integer.valueOf(rs.getString(3));
		}
		rs.close();
		st.close();
		
		System.out.println("	ID	|	EMAIL	|	INGRESOS");
		for (Registros i : arrayListRegistros) {
			System.out.println(i);
		}
		
        long fin = System.nanoTime();  
        double tiempo = (double) ((fin - inicio)/100000000);
        System.out.println("Total ingresos: "+ cnt);
        System.out.println("Tiempo empleado: "+ tiempo/10 +" segundos."); //El tiempo siempre sale 0.0
		
	}

}
