package utilidades;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

public class GestorBD {

	private static Connection con = null;

	public GestorBD() {
		
	}

	public static java.sql.Connection conectarBBDD() throws Exception {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			//Para conectar en linux
			//con = DriverManager.getConnection("jdbc:mysql://localhost/BBDD_PSP_1", "root", "manager");
			//para conectar en windows
			con = DriverManager.getConnection("jdbc:mysql://localhost/BBDD_PSP_1", "root", "");
			
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Error al conectar con la BBDD", "Error de conexion",
					JOptionPane.ERROR_MESSAGE);
		}
		return con;
	}
	
	/**
	 * Metodo para registrar individuos
	 * @param res
	 * @throws SQLException
	 */
	public static void registrar(int res) throws SQLException {
		java.sql.Connection conexion = con;		
		java.sql.PreparedStatement pst;
		String insert="INSERT INTO T_INDIVIDUOS (email, ingresos) VALUES ('', "+res+")";		
		pst = conexion.prepareStatement(insert);
		pst.executeUpdate();
		pst.close();
	}

}
