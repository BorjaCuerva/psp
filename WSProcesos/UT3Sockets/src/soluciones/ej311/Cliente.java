package soluciones.ej311;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Cliente{
	public static void main(String[] args) throws IOException{
		String host="localhost";
		int nPuerto=5000;
		Socket cliente=null;
		
		Scanner sc= new Scanner(System.in);
		String entrada;
		int contador=1;
		
		while(true){
			System.out.println("Introduce cadena "+contador+":");
			cliente=new Socket(host, nPuerto);
			
			//Creamos flujo de salida al servidor
			DataOutputStream flujoSalida=new DataOutputStream(cliente.getOutputStream());
			
			entrada=sc.next();
			
			flujoSalida.writeBytes(entrada);
			
			
			PrintWriter printWriter=new PrintWriter(flujoSalida, true);
			printWriter.println(entrada);
			
			
			//Creamos flujo de entrada al servidor
			DataInputStream flujoEntrada=new DataInputStream(cliente.getInputStream());
			System.out.println("Recibiendo datos del SERVIDOR...");
			System.out.println(flujoEntrada.readUTF());
			
			//Cerramos streams y sockets
			flujoEntrada.close();
			flujoSalida.close();
			printWriter.close();
			
			contador++;
		}
	}
}