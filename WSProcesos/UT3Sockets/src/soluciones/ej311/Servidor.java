package soluciones.ej311;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Servidor{
	public static void main(String[] args) throws IOException{
		int nPuerto=5000;
		ServerSocket servidor=null;
		Socket clienteConectado=null;
		servidor=new ServerSocket(nPuerto);
		
		while(true){
			System.out.println("Esperando al cliente ...");
			clienteConectado=servidor.accept();
			
			//Creamos flujo de entrada del cliente
			InputStream entrada=null;
			entrada=clienteConectado.getInputStream();
			DataInputStream flujoEntrada= new DataInputStream(entrada);
			
			BufferedReader br=new BufferedReader(new InputStreamReader(flujoEntrada));
			
			String entrada2=br.readLine();
			System.out.println("He recibido " + entrada2);
			
			System.out.println("Recibiendo: "+entrada2);
			
			//Creamos flujo de salida al cliente
			OutputStream salida=null;
			salida=clienteConectado.getOutputStream();
			DataOutputStream flujoSalida=new DataOutputStream(salida);
			
			//Envio datos al cliente
			flujoSalida.writeUTF(entrada2);
			
			//Cerramos streams y sockets
			entrada.close();
			flujoEntrada.close();
		}
	}
}