package ejercicios2_1;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;

public class Servidor {

	public static void main(String[] args) {
		
		System.out.println("Creación del socket servidor.");
		try {
			ServerSocket serverSocket = new ServerSocket();
			InetSocketAddress isa = new InetSocketAddress("localhost",5000);
			serverSocket.bind(isa);
			System.out.println ("Espera a que llegue una petición de socket");
			Socket socket = serverSocket.accept();
			System.out.println ("Se ha establecido la conexión");
			
			InputStream is = socket.getInputStream();
            OutputStream os = socket.getOutputStream();
            
            //Numero de cliente
            Random r = new Random();
            int numeroCliente = r.nextInt(6)+1;  // Entre 0 y 5, más 1.
            System.out.println("El número de cliente es: "+numeroCliente);
            os.write(numeroCliente);
			
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
