package ejercicios2_1;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;

public class Cliente {

	public static void main(String[] args) {
		
		
		try {
			System.out.println("Creando socket del cliente");
			Socket clientSocket = new Socket();
			System.out.println("Estableciendo conexión");
			InetSocketAddress addr = new InetSocketAddress("localhost", 5000);
			clientSocket.connect(addr);
			
			InputStream is = clientSocket.getInputStream();
			OutputStream os = clientSocket.getOutputStream();
			
			byte[] mensaje = new byte[14];
			is.read(mensaje);
			
			System.out.println("Mi numero de cliente es: "+new String(mensaje));

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
