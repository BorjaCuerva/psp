package ejercicios1_1;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class Servidor {

	public static void main(String[] args) {
		
		
		try {
			System.out.println ("Creación del socket servidor");
			ServerSocket serverSocket = new ServerSocket();
			System.out.println ("Realización del bind");
			InetSocketAddress isa = new InetSocketAddress("localhost",5000);
			serverSocket.bind(isa);
			System.out.println ("Espera a que llegue una petición de socket");
			Socket socket = serverSocket.accept();
			System.out.println ("Se ha establecido la conexión");
			
			InputStream is = socket.getInputStream();
            OutputStream os = socket.getOutputStream();
            
            byte[] mensaje = new byte[14];
			is.read(mensaje);
			System.out.println("Mensaje recibido.");
			
			
			os.write(mensaje);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

}
