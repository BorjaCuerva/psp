package ejercicios1_1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Scanner;

public class Cliente {

	public static void main(String[] args) {

		String cadena;
		char theChar = 0;

		try {
			//Creamos socket cliente
			System.out.println("Creando socket cliente");
			Socket clientSocket = new Socket();
			System.out.println("Estableciendo conexión");
			InetSocketAddress addr = new InetSocketAddress("localhost", 5000);
			clientSocket.connect(addr);
			
			InputStream is = clientSocket.getInputStream();
			OutputStream os = clientSocket.getOutputStream();

			Scanner sn = new Scanner(System.in);
			byte[] mensaje = new byte[14];
			
			System.out.print("Introduce cadena: ");
			mensaje = sn.next().getBytes();
			
			
			os.write(mensaje);
			System.out.println("Mensaje del servidor: " + new String(mensaje));
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
